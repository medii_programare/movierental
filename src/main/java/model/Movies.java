package model;

/**
 * Created by Dandu on 6/21/2017.
 */
public class Movies {
    private String name;
    private int year;

    public Movies(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Movies{" +
                "name='" + name + '\'' +
                ", year=" + year +
                '}';
    }
}
