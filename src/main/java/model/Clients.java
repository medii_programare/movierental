package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dandu on 6/21/2017.
 */
public class Clients {
    private String name;
    private int age;
    private List<Movies> rentals = new ArrayList<Movies>();


    public Clients(String name, int age) {
        this.name = name;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public List<Movies> getRentals() {
        return rentals;
    }

    @Override
    public String toString() {
        return "Clients{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", rentals=" + rentals +
                '}';
    }

    public void addRental(Movies m) {
        rentals.add(m);
    }
}
