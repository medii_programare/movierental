import model.Clients;
import repository.CRepo;
import repository.CRepository;

/**
 * Created by Dandu on 6/21/2017.
 */
public class Main {
    //    create a few clients
    public static void main(String[] args) {
        Clients ion = new Clients("Ion", 30);
        Clients ana = new Clients("Ana", 22);

        CRepo<Clients> crepo = new CRepository<Clients>();

        try {
            crepo.add(ion);
            crepo.add(ana);
            crepo.add(ana);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
