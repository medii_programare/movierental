package repository;

import model.Movies;

/**
 * Created by Dandu on 6/21/2017.
 */
public interface CRepo<Clients> {
    void add(Clients c) throws Exception;
    void remove(Clients c) throws Exception;
    void update(Clients c) throws Exception;
    void rent(Clients c, Movies m) throws Exception;

}
