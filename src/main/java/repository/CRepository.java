package repository;

import model.Movies;

import java.util.List;

/**
 * Created by Dandu on 6/21/2017.
 */
public class CRepository<Clients> implements CRepo<Clients> {

    private List<Clients> clients;

    public void add(Clients c) throws Exception {
        if (clients.contains(c))
            throw new Exception("Existing client");
        clients.add(c);
    }

    public void remove(Clients c) throws Exception {
        if (!clients.contains(c))
            throw new Exception("Client does not exist");
        clients.remove(c);
    }

    public void update(Clients c) throws Exception {
//        what
    }

    public void rent(Clients c, Movies m) throws Exception {
//        what
    }
}
