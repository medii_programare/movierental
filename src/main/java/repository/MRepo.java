package repository;

import java.util.List;

/**
 * Created by Dandu on 6/21/2017.
 * Declares function headings for CRUD operations with Movies type objects
 */
public interface MRepo<M> {
    void add(M m) throws Exception;
    void remove(M m) throws Exception;
    void update(M m) throws Exception;
    int size();
    List<M> getAll();
    boolean search(M m) throws Exception;
}
